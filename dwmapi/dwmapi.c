#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <dwmapi.h>
#include "link.h"

/* Fix for IE and Sidebar */
HRESULT WINAPI _DwmExtendFrameIntoClientArea(HWND hWnd,
    _In_ const MARGINS *pMarInset)
{
    UNREFERENCED_PARAMETER(hWnd);
    UNREFERENCED_PARAMETER(pMarInset);

    return 0xC0000001;  /* STATUS_UNSUCCESSFUL */
}

/* Fix for Photo Viewer */
HRESULT WINAPI _DwmIsCompositionEnabled(_Out_ BOOL *pfEnabled)
{
    *pfEnabled = FALSE;
    return S_OK;
}

BOOL WINAPI __IsCompositionActive(void)
{
    return FALSE;
}

static
void PatchCode(HMODULE hDll, char *fname, void *newfunc)
{
#ifdef _WIN64
    /* mov rax,x; jmp [rax]; */
    char code[] = { 0x48, 0xB8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                    0xFF, 0xE0 };
#else
    /* mov eax,x; jmp [eax]; */
    char code[] = { 0xB8, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xE0 };
#endif
    void *oldfunc;
    DWORD oldprot;

    oldfunc = (void *)GetProcAddress(hDll, fname);

#ifdef _WIN64
    *(void **)(code + 2) = newfunc;
#else
    *(void **)(code + 1) = newfunc;
#endif

    VirtualProtect(oldfunc, sizeof(code), PAGE_EXECUTE_READWRITE, &oldprot);
    memcpy(oldfunc, code, sizeof(code));
    VirtualProtect(oldfunc, sizeof(code), oldprot, &oldprot);
}

BOOL WINAPI _main(HINSTANCE hinstDLL, DWORD fdwReason, void *lpvReserved)
{
    UNREFERENCED_PARAMETER(hinstDLL);
    UNREFERENCED_PARAMETER(lpvReserved);

    HMODULE hDll;

    if (fdwReason != DLL_PROCESS_ATTACH) return TRUE;

    /* fix IE tabs */
    hDll = LoadLibraryW(L"uxtheme.dll");
    PatchCode(hDll, "IsCompositionActive", (void *)__IsCompositionActive);

    return TRUE;
}
