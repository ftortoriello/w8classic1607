/* Export all original functions (v10.0.14393.206), and the modified ones.
 * Exporting the original functions fixes Photo Viewer transparency and
 * launching Explorer.
 *
 * Rename the original system "dwmapi.dll" to "dwmapio.dll".
 */

#pragma comment(linker, "/export:DwmpDxGetWindowSharedSurface=dwmapio.dll.DwmpDxGetWindowSharedSurface,@100")
#pragma comment(linker, "/export:DwmpDxUpdateWindowSharedSurface=dwmapio.dll.DwmpDxUpdateWindowSharedSurface,@101")
#pragma comment(linker, "/export:DwmEnableComposition=dwmapio.dll.DwmEnableComposition,@102")
#pragma comment(linker, "/export:ord103=dwmapio.dll.#103,@103,NONAME")
#pragma comment(linker, "/export:ord104=dwmapio.dll.#104,@104,NONAME")
#pragma comment(linker, "/export:ord105=dwmapio.dll.#105,@105,NONAME")
#pragma comment(linker, "/export:ord106=dwmapio.dll.#106,@106,NONAME")
#pragma comment(linker, "/export:ord107=dwmapio.dll.#107,@107,NONAME")
#pragma comment(linker, "/export:ord108=dwmapio.dll.#108,@108,NONAME")
#pragma comment(linker, "/export:ord109=dwmapio.dll.#109,@109,NONAME")
#pragma comment(linker, "/export:ord110=dwmapio.dll.#110,@110,NONAME")
/* #pragma comment(linker, "/export:DllCanUnloadNow=dwmapio.dll.DllCanUnloadNow,@111") */
#pragma comment(linker, "/export:ord112=dwmapio.dll.#112,@112,NONAME")
#pragma comment(linker, "/export:ord113=dwmapio.dll.#113,@113,NONAME")
#pragma comment(linker, "/export:ord114=dwmapio.dll.#114,@114,NONAME")
/* #pragma comment(linker, "/export:DllGetClassObject=dwmapio.dll.DllGetClassObject,@115") */
#pragma comment(linker, "/export:DwmAttachMilContent=dwmapio.dll.DwmAttachMilContent,@116")
#pragma comment(linker, "/export:DwmDefWindowProc=dwmapio.dll.DwmDefWindowProc,@117")
#pragma comment(linker, "/export:DwmDetachMilContent=dwmapio.dll.DwmDetachMilContent,@118")
#pragma comment(linker, "/export:DwmEnableBlurBehindWindow=dwmapio.dll.DwmEnableBlurBehindWindow,@119")
#pragma comment(linker, "/export:DwmEnableMMCSS=dwmapio.dll.DwmEnableMMCSS,@120")
/* #pragma comment(linker, "/export:DwmExtendFrameIntoClientArea=dwmapio.dll.DwmExtendFrameIntoClientArea,@121") */
/* #pragma comment(linker, "/export:DwmExtendFrameIntoClientArea=_DwmExtendFrameIntoClientArea,@121") */
#pragma comment(linker, "/export:DwmFlush=dwmapio.dll.DwmFlush,@122")
#pragma comment(linker, "/export:DwmGetColorizationColor=dwmapio.dll.DwmGetColorizationColor,@123")
#pragma comment(linker, "/export:ord124=dwmapio.dll.#124,@124,NONAME")

#if WINBUILD >= 20348
#pragma comment(linker, "/export:DwmGetCompositionTimingInfo=dwmapio.dll.DwmGetCompositionTimingInfo,@125")
#pragma comment(linker, "/export:DwmGetGraphicsStreamClient=dwmapio.dll.DwmGetGraphicsStreamClient,@126")
#else
#pragma comment(linker, "/export:DwmpDxBindSwapChain=dwmapio.dll.DwmpDxBindSwapChain,@125")
#pragma comment(linker, "/export:DwmpDxUnbindSwapChain=dwmapio.dll.DwmpDxUnbindSwapChain,@126")
#endif

#if WINBUILD >= 17763
#pragma comment(linker, "/export:DwmpGetColorizationParameters=dwmapio.dll.DwmpGetColorizationParameters,@127")
#else
#pragma comment(linker, "/export:ord127=dwmapio.dll.#127,@127,NONAME")
#endif

#if WINBUILD >= 20348
#pragma comment(linker, "/export:DwmpDxgiIsThreadDesktopComposited=dwmapio.dll.DwmpDxgiIsThreadDesktopComposited,@128")
#pragma comment(linker, "/export:DwmGetGraphicsStreamTransformHint=dwmapio.dll.DwmGetGraphicsStreamTransformHint,@129")
#pragma comment(linker, "/export:DwmGetTransportAttributes=dwmapio.dll.DwmGetTransportAttributes,@130")
#else
#pragma comment(linker, "/export:DwmGetGraphicsStreamTransformHint=dwmapio.dll.DwmGetGraphicsStreamTransformHint,@149")
#pragma comment(linker, "/export:DwmGetCompositionTimingInfo=dwmapio.dll.DwmGetCompositionTimingInfo,@129")
#pragma comment(linker, "/export:DwmGetGraphicsStreamClient=dwmapio.dll.DwmGetGraphicsStreamClient,@130")
#endif

#if WINBUILD >= 17763
#pragma comment(linker, "/export:DwmpSetColorizationParameters=dwmapio.dll.DwmpSetColorizationParameters,@131")
#else
#pragma comment(linker, "/export:ord131=dwmapio.dll.#131,@131,NONAME")
#endif

#pragma comment(linker, "/export:ord132=dwmapio.dll.#132,@132,NONAME")

#if WINBUILD >= 20348
#pragma comment(linker, "/export:DwmGetUnmetTabRequirements=dwmapio.dll.DwmGetUnmetTabRequirements,@133")
#pragma comment(linker, "/export:DwmGetWindowAttribute=dwmapio.dll.DwmGetWindowAttribute,@134")
#else
#pragma comment(linker, "/export:DwmpDxUpdateWindowRedirectionBltSurface=dwmapio.dll.DwmpDxUpdateWindowRedirectionBltSurface,@133")
#pragma comment(linker, "/export:ord134=dwmapio.dll.#134,@134,NONAME")
#endif

#pragma comment(linker, "/export:DwmpRenderFlick=dwmapio.dll.DwmpRenderFlick,@135")
#pragma comment(linker, "/export:DwmpAllocateSecurityDescriptor=dwmapio.dll.DwmpAllocateSecurityDescriptor,@136")
#pragma comment(linker, "/export:DwmpFreeSecurityDescriptor=dwmapio.dll.DwmpFreeSecurityDescriptor,@137")
#pragma comment(linker, "/export:ord138=dwmapio.dll.#138,@138,NONAME")
#pragma comment(linker, "/export:ord139=dwmapio.dll.#139,@139,NONAME")
#pragma comment(linker, "/export:ord140=dwmapio.dll.#140,@140,NONAME")
#pragma comment(linker, "/export:ord141=dwmapio.dll.#141,@141,NONAME")
#pragma comment(linker, "/export:ord142=dwmapio.dll.#142,@142,NONAME")
#pragma comment(linker, "/export:DwmpEnableDDASupport=dwmapio.dll.DwmpEnableDDASupport,@143")
#pragma comment(linker, "/export:ord144=dwmapio.dll.#144,@144,NONAME")
#pragma comment(linker, "/export:ord145=dwmapio.dll.#145,@145,NONAME")
#pragma comment(linker, "/export:ord146=dwmapio.dll.#146,@146,NONAME")
#pragma comment(linker, "/export:ord147=dwmapio.dll.#147,@147,NONAME")
#pragma comment(linker, "/export:ord148=dwmapio.dll.#148,@148,NONAME")

#if WINBUILD >= 20348
#pragma comment(linker, "/export:DwmInvalidateIconicBitmaps=dwmapio.dll.DwmInvalidateIconicBitmaps,@149")
#else
#pragma comment(linker, "/export:DwmGetGraphicsStreamTransformHint=dwmapio.dll.DwmGetGraphicsStreamTransformHint,@149")
#endif

#pragma comment(linker, "/export:ord150=dwmapio.dll.#150,@150,NONAME")
#pragma comment(linker, "/export:ord151=dwmapio.dll.#151,@151,NONAME")
#pragma comment(linker, "/export:ord152=dwmapio.dll.#152,@152,NONAME")
#pragma comment(linker, "/export:ord153=dwmapio.dll.#153,@153,NONAME")
#pragma comment(linker, "/export:ord154=dwmapio.dll.#154,@154,NONAME")
#pragma comment(linker, "/export:ord155=dwmapio.dll.#155,@155,NONAME")
#pragma comment(linker, "/export:DwmTetherTextContact=dwmapio.dll.DwmTetherTextContact,@156")
#pragma comment(linker, "/export:ord157=dwmapio.dll.#157,@157,NONAME")
#pragma comment(linker, "/export:ord158=dwmapio.dll.#158,@158,NONAME")
#pragma comment(linker, "/export:ord159=dwmapio.dll.#159,@159,NONAME")
#pragma comment(linker, "/export:ord160=dwmapio.dll.#160,@160,NONAME")
#pragma comment(linker, "/export:ord161=dwmapio.dll.#161,@161,NONAME")
#pragma comment(linker, "/export:ord162=dwmapio.dll.#162,@162,NONAME")
#pragma comment(linker, "/export:ord163=dwmapio.dll.#163,@163,NONAME")
#pragma comment(linker, "/export:ord164=dwmapio.dll.#164,@164,NONAME")

#if WINBUILD >= 17763
#pragma comment(linker, "/export:ord165=dwmapio.dll.#165,@165,NONAME")
#pragma comment(linker, "/export:ord166=dwmapio.dll.#166,@166,NONAME")
#pragma comment(linker, "/export:ord167=dwmapio.dll.#167,@167,NONAME")
#pragma comment(linker, "/export:ord168=dwmapio.dll.#168,@168,NONAME")
#pragma comment(linker, "/export:ord169=dwmapio.dll.#169,@169,NONAME")
#pragma comment(linker, "/export:ord170=dwmapio.dll.#170,@170,NONAME")
#pragma comment(linker, "/export:ord171=dwmapio.dll.#171,@171,NONAME")
#pragma comment(linker, "/export:ord172=dwmapio.dll.#172,@172,NONAME")
#pragma comment(linker, "/export:ord173=dwmapio.dll.#173,@173,NONAME")
#pragma comment(linker, "/export:ord174=dwmapio.dll.#174,@174,NONAME")
#pragma comment(linker, "/export:ord175=dwmapio.dll.#175,@175,NONAME")
#pragma comment(linker, "/export:ord176=dwmapio.dll.#176,@176,NONAME")
#pragma comment(linker, "/export:ord177=dwmapio.dll.#177,@177,NONAME")
#pragma comment(linker, "/export:ord178=dwmapio.dll.#178,@178,NONAME")
#pragma comment(linker, "/export:ord179=dwmapio.dll.#179,@179,NONAME")
#pragma comment(linker, "/export:ord180=dwmapio.dll.#180,@180,NONAME")
#endif

#if WINBUILD >= 20348

#pragma comment(linker, "/export:ord181=dwmapio.dll.#181,@181,NONAME")
#pragma comment(linker, "/export:ord182=dwmapio.dll.#182,@182,NONAME")
#pragma comment(linker, "/export:DwmpUpdateProxyWindowForCapture=dwmapio.dll.DwmpUpdateProxyWindowForCapture,@183")
#pragma comment(linker, "/export:ord184=dwmapio.dll.#184,@184,NONAME")
#pragma comment(linker, "/export:ord185=dwmapio.dll.#185,@185,NONAME")
/* #pragma comment(linker, "/export:DwmIsCompositionEnabled=dwmapio.dll.DwmIsCompositionEnabled,@186") */
/* #pragma comment(linker, "/export:DwmIsCompositionEnabled=_DwmIsCompositionEnabled,@186") */
#pragma comment(linker, "/export:DwmModifyPreviousDxFrameDuration=dwmapio.dll.DwmModifyPreviousDxFrameDuration,@187")
#pragma comment(linker, "/export:DwmQueryThumbnailSourceSize=dwmapio.dll.DwmQueryThumbnailSourceSize,@188")
#pragma comment(linker, "/export:DwmRegisterThumbnail=dwmapio.dll.DwmRegisterThumbnail,@189")
#pragma comment(linker, "/export:DwmRenderGesture=dwmapio.dll.DwmRenderGesture,@190")
#pragma comment(linker, "/export:DwmSetDxFrameDuration=dwmapio.dll.DwmSetDxFrameDuration,@191")
#pragma comment(linker, "/export:DwmSetIconicLivePreviewBitmap=dwmapio.dll.DwmSetIconicLivePreviewBitmap,@192")
#pragma comment(linker, "/export:DwmSetIconicThumbnail=dwmapio.dll.DwmSetIconicThumbnail,@193")
#pragma comment(linker, "/export:DwmSetPresentParameters=dwmapio.dll.DwmSetPresentParameters,@194")
#pragma comment(linker, "/export:DwmSetWindowAttribute=dwmapio.dll.DwmSetWindowAttribute,@195")
#pragma comment(linker, "/export:DwmShowContact=dwmapio.dll.DwmShowContact,@196")
#pragma comment(linker, "/export:DwmTetherContact=dwmapio.dll.DwmTetherContact,@197")
#pragma comment(linker, "/export:DwmTransitionOwnedWindow=dwmapio.dll.DwmTransitionOwnedWindow,@198")
#pragma comment(linker, "/export:DwmUnregisterThumbnail=dwmapio.dll.DwmUnregisterThumbnail,@199")
#pragma comment(linker, "/export:DwmUpdateThumbnailProperties=dwmapio.dll.DwmUpdateThumbnailProperties,@200")

#elif WINBUILD >= 17763

#pragma comment(linker, "/export:DwmGetTransportAttributes=dwmapio.dll.DwmGetTransportAttributes,@181")
#pragma comment(linker, "/export:DwmGetUnmetTabRequirements=dwmapio.dll.DwmGetUnmetTabRequirements,@182")
#pragma comment(linker, "/export:DwmGetWindowAttribute=dwmapio.dll.DwmGetWindowAttribute,@183")
#pragma comment(linker, "/export:DwmInvalidateIconicBitmaps=dwmapio.dll.DwmInvalidateIconicBitmaps,@184")
/* #pragma comment(linker, "/export:DwmIsCompositionEnabled=dwmapio.dll.DwmIsCompositionEnabled,@185") */
/* #pragma comment(linker, "/export:DwmIsCompositionEnabled=_DwmIsCompositionEnabled,@185") */
#pragma comment(linker, "/export:DwmModifyPreviousDxFrameDuration=dwmapio.dll.DwmModifyPreviousDxFrameDuration,@186")
#pragma comment(linker, "/export:DwmQueryThumbnailSourceSize=dwmapio.dll.DwmQueryThumbnailSourceSize,@187")
#pragma comment(linker, "/export:DwmRegisterThumbnail=dwmapio.dll.DwmRegisterThumbnail,@188")
#pragma comment(linker, "/export:DwmRenderGesture=dwmapio.dll.DwmRenderGesture,@189")
#pragma comment(linker, "/export:ord190=dwmapio.dll.#190,@190,NONAME")
#pragma comment(linker, "/export:DwmSetDxFrameDuration=dwmapio.dll.DwmSetDxFrameDuration,@191")
#pragma comment(linker, "/export:DwmSetIconicLivePreviewBitmap=dwmapio.dll.DwmSetIconicLivePreviewBitmap,@192")
#pragma comment(linker, "/export:DwmSetIconicThumbnail=dwmapio.dll.DwmSetIconicThumbnail,@193")
#pragma comment(linker, "/export:DwmSetPresentParameters=dwmapio.dll.DwmSetPresentParameters,@194")
#pragma comment(linker, "/export:DwmSetWindowAttribute=dwmapio.dll.DwmSetWindowAttribute,@195")
#pragma comment(linker, "/export:DwmShowContact=dwmapio.dll.DwmShowContact,@196")
#pragma comment(linker, "/export:DwmTetherContact=dwmapio.dll.DwmTetherContact,@197")
#pragma comment(linker, "/export:DwmTransitionOwnedWindow=dwmapio.dll.DwmTransitionOwnedWindow,@198")
#pragma comment(linker, "/export:DwmUnregisterThumbnail=dwmapio.dll.DwmUnregisterThumbnail,@199")
#pragma comment(linker, "/export:DwmUpdateThumbnailProperties=dwmapio.dll.DwmUpdateThumbnailProperties,@200")

#else  /* WINBUILD < 17763 */

#pragma comment(linker, "/export:DwmGetTransportAttributes=dwmapio.dll.DwmGetTransportAttributes,@165")
#pragma comment(linker, "/export:DwmGetWindowAttribute=dwmapio.dll.DwmGetWindowAttribute,@166")
#pragma comment(linker, "/export:DwmInvalidateIconicBitmaps=dwmapio.dll.DwmInvalidateIconicBitmaps,@167")
/* #pragma comment(linker, "/export:DwmIsCompositionEnabled=dwmapio.dll.DwmIsCompositionEnabled,@168") */
/* #pragma comment(linker, "/export:DwmIsCompositionEnabled=_DwmIsCompositionEnabled,@168") */
#pragma comment(linker, "/export:DwmModifyPreviousDxFrameDuration=dwmapio.dll.DwmModifyPreviousDxFrameDuration,@169")
#pragma comment(linker, "/export:DwmQueryThumbnailSourceSize=dwmapio.dll.DwmQueryThumbnailSourceSize,@170")
#pragma comment(linker, "/export:DwmRegisterThumbnail=dwmapio.dll.DwmRegisterThumbnail,@171")
#pragma comment(linker, "/export:DwmRenderGesture=dwmapio.dll.DwmRenderGesture,@172")
#pragma comment(linker, "/export:DwmSetDxFrameDuration=dwmapio.dll.DwmSetDxFrameDuration,@173")
#pragma comment(linker, "/export:DwmSetIconicLivePreviewBitmap=dwmapio.dll.DwmSetIconicLivePreviewBitmap,@174")
#pragma comment(linker, "/export:DwmSetIconicThumbnail=dwmapio.dll.DwmSetIconicThumbnail,@175")
#pragma comment(linker, "/export:DwmSetPresentParameters=dwmapio.dll.DwmSetPresentParameters,@176")
#pragma comment(linker, "/export:DwmSetWindowAttribute=dwmapio.dll.DwmSetWindowAttribute,@177")
#pragma comment(linker, "/export:DwmShowContact=dwmapio.dll.DwmShowContact,@178")
#pragma comment(linker, "/export:DwmTetherContact=dwmapio.dll.DwmTetherContact,@179")
#pragma comment(linker, "/export:DwmTransitionOwnedWindow=dwmapio.dll.DwmTransitionOwnedWindow,@180")
#pragma comment(linker, "/export:DwmUnregisterThumbnail=dwmapio.dll.DwmUnregisterThumbnail,@181")
#pragma comment(linker, "/export:DwmUpdateThumbnailProperties=dwmapio.dll.DwmUpdateThumbnailProperties,@182")

#endif
