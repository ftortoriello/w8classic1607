#define WIN32_NO_STATUS
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <tlhelp32.h>
#include <shlwapi.h>
#include <ntndk.h>
#include <strsafe.h>

static WCHAR *ServiceGUID = L" ca18cc4e-b373-495b-91a3-3c2a8d53e12d";

static
DWORD GetPID(WCHAR *name)
{
    HANDLE hToolHelp = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (!hToolHelp)
        return 0;

    PROCESSENTRY32W pe;
    pe.dwSize = sizeof(pe);

    Process32FirstW(hToolHelp, &pe);
    do
    {
        if (0 == lstrcmpiW(pe.szExeFile, name))
            return pe.th32ProcessID;
    }
    while (Process32NextW(hToolHelp, &pe));

    return 0;
}

static
SYSTEM_HANDLE_INFORMATION *GetGlobalHandles(void)
{
    WCHAR tmp[256];
    SIZE_T resultLength = 0;

    NTSTATUS ret = NtQuerySystemInformation(SystemHandleInformation, tmp,
        sizeof(SYSTEM_HANDLE_INFORMATION), &resultLength);
    if (!(ret == STATUS_INFO_LENGTH_MISMATCH && resultLength > 0))
        return NULL;

    SYSTEM_HANDLE_INFORMATION *pSysHandleInfo = (SYSTEM_HANDLE_INFORMATION *)
        HeapAlloc(GetProcessHeap(), 0, resultLength);
    if (!pSysHandleInfo)
        return NULL;

    NtQuerySystemInformation(SystemHandleInformation, pSysHandleInfo,
        resultLength, NULL);
    return pSysHandleInfo;
}

static
WCHAR *GetHandleName(HANDLE hProcess, HANDLE hSourceHandle)
{
    HANDLE handle = NULL;
    WCHAR handleName[2048];

    /* Duplicate the handle to get its name */
    DuplicateHandle(hProcess, hSourceHandle, GetCurrentProcess(), &handle, 0,
        FALSE, DUPLICATE_SAME_ACCESS);
    /* Get name */
    NtQueryObject(handle, ObjectNameInformation,
        &handleName, sizeof(handleName), NULL);
    CloseHandle(handle);

    int handleNameLength = ((UNICODE_STRING *)handleName)->Length;
    if (handleNameLength <= 0)
        return L"";

    WCHAR *handleNameBuffer = ((UNICODE_STRING *)handleName)->Buffer;
    /* Add null string termination */
    *(WORD *)(handleNameBuffer + handleNameLength) = 0;
    return handleNameBuffer;
}

static
BOOL SeekAndDestroyThemeSection(HANDLE hProcess, DWORD pid,
    SYSTEM_HANDLE_INFORMATION *pSysHandleInfo)
{
    for (UINT i = 0; i < pSysHandleInfo->NumberOfHandles; i++)
    {
        if (pSysHandleInfo->Handles[i].UniqueProcessId == pid)
        {
            HANDLE hSourceHandle = (HANDLE)pSysHandleInfo->Handles[i].HandleValue;
            WCHAR *name = GetHandleName(hProcess, hSourceHandle);
            if (StrStrIW(name, L"\\ThemeSection"))
            {
                /* Destroy handle */
                DuplicateHandle(hProcess, hSourceHandle, GetCurrentProcess(), NULL, 0,
                    FALSE, DUPLICATE_CLOSE_SOURCE);
                return TRUE;
            }
        }
    }
    return FALSE;
}

static
UINT MainService(void)
{
    DWORD pid = GetPID(L"WinLogon.exe");
    if (0 == pid)
        return 1;

    SYSTEM_HANDLE_INFORMATION *pSysHandleInfo = GetGlobalHandles();
    if (!pSysHandleInfo)
        return 1;

    UINT ret = 0;

    HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
    if (hProcess)
    {
        if (SeekAndDestroyThemeSection(hProcess, pid, pSysHandleInfo) == FALSE)
            ret = 1;
        CloseHandle(hProcess);
    }
    else
        ret = 1;

    HeapFree(GetProcessHeap(), 0, pSysHandleInfo);
    return ret;
}

static
UINT RunAsService(void)
{
    SC_HANDLE hSCManager = OpenSCManagerW(NULL, NULL,
        SC_MANAGER_CREATE_SERVICE);
    if (!hSCManager)
        return 1;

    WCHAR path[MAX_PATH];
    GetModuleFileNameW(GetModuleHandleW(NULL), path, sizeof(path));
    StringCbCatW(path, sizeof(path), ServiceGUID);

    const WCHAR *PROGNAME = L"ClassicTheme";
    SC_HANDLE hService = CreateServiceW(hSCManager, PROGNAME, PROGNAME,
        SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS, SERVICE_DEMAND_START,
        SERVICE_ERROR_IGNORE, path, NULL, NULL, NULL, NULL, NULL);

    UINT ret;

    if (hService)
    {
        /* TODO FIXME: Event 7009:
         * A timeout was reached (30000 milliseconds) while waiting for the
         * ClassicTheme service to connect.
         * Sleeping does not help...
         */
        StartServiceW(hService, 0, NULL);

        SERVICE_STATUS sst;
        ControlService(hService, SERVICE_CONTROL_STOP, &sst);

        DeleteService(hService);
        CloseServiceHandle(hService);
        ret = 0;
    }
    else
        ret = 1;

    CloseServiceHandle(hSCManager);
    return ret;
}

int WINAPI _main(void)
{
    WCHAR *cmdl = GetCommandLineW();

    if (StrStrW(cmdl, ServiceGUID))
        ExitProcess(MainService());
    else
        ExitProcess(RunAsService());
}
