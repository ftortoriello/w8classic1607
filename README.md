# w8classic

Fork of w8classic, cleaned-up, fixed and optimized **specifically** for Windows
10 Anniversary Update (version 1607, build 10.0.14393).

It includes the software to enable the classic theme (not many changes were
made there), and the dwmapi wrapper for version 1607, which is used to fix
visual and other issues when using the Desktop Window Manager and the Classic
Theme (in software like the classic Photo Viewer and Internet Explorer).

Usage: To fix it the issue system-wide, rename the original dwmapi.dll to
dwmapio.dll, in both Windows\System32 and Windows\SysWOW64 (on a 64-bit OS).

**WARNING**: Install this only if you know what you are doing.

